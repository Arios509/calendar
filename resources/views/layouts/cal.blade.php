<!DOCTYPE html>
<html>
<head>
    <meta charset='utf-8' />
    <link rel='stylesheet' href='css/lib/cupertino/jquery-ui.min.css' />
    <link href='css/fullcalendar.css' rel='stylesheet' />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel='stylesheet' type='text/css'>

    <!-- Styles -->
    {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    {{--<link href='css/fullcalendar.print.css' rel='stylesheet' media='print' />--}}
    <link rel='stylesheet' href='css/lib/cupertino/jquery-ui.min.css' />
    <link href='css/fullcalendar.css' rel='stylesheet' />
    <link href='css/fullcalendar.print.css' rel='stylesheet' media='print' />


</head>
<body>



@yield('content1')

@if (Auth::user())
    <div id="calendar"></div>
@endif

<script src='js/lib/moment.min.js'></script>
<script src='js/lib/jquery.min.js'></script>
<script src='js/fullcalendar.min.js'></script>

<script>
    $(document).ready(function() {
        // Check calender div is exist or not.
        // In case of guest user it will not present
        if($("#calendar").length ){
            $('#calendar').fullCalendar({

                theme: true,
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,agendaWeek,agendaDay'
                },
                selectable:true,
                selectHelper:true,


                eventClick:function(event,delta,revertfunc){
                  var title = event.title;
                    var start = event.start.format("YYYY-MM-DD[T]HH:mm;SS");
                    $.ajax({
                        url:'EventsController@i'
                    })


                },


                select: function(start, end){
                    var title = prompt('Event Title :');
                    var eventData;
                    if(title){
                        eventData = {
                            title: title,
                            start: start,
                            end:end
                        };
                        $('#calendar').fullCalendar('renderEvent',eventData,true);
                    }
                    $('#calendar').fullCalendar('unselect');
                },
                editable:true,
                eventLimit:true
            });
        }

    });
</script>


</body>
</html>