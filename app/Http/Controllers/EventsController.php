<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class EventsController extends Controller
{
    //
    protected function create(array $data){
        return Events::create([
            'title' => $data['calendar_title'],
            'start_date' => $data['created_at'],

        ]);
    }
}
